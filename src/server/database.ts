import Datastore from 'nedb-promises'

const path = require('path')

const fs = require('fs')

try {
	if (!fs.existsSync(path.join('./db'))) {
		fs.mkdir(path.join('./db'), (err: any) => {
			if (err) {
				throw err
			}
			fs.open('db/database.db', 'w', (error: any, file: any) => {
				if (err) throw err
				console.log('Saved file!')
			})
		})
	}
} catch (err) {
	console.error(err)
}

const datastore = Datastore.create(path.join('./db/database.db'))
if (process.platform === 'win32') {
	if (fs.existsSync(path.join('./db/database.db'))) {
		console.log('Database has been set up')
	} else {
		console.log('Something went wrong with setting up database')
	}
} else if (process.platform === 'linux') {
	if (fs.existsSync(path.join('db/database.db'))) {
		console.log('Database has been set up')
	} else {
		console.log('Something went wrong with setting up database')
	}
}

export default datastore
