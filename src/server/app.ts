const express = require('express')
const cors = require('cors')
const helmet = require('helmet')
const compression = require('compression')
const bodyparser = require('body-parser')

export default class App 
{
    public app: any
    
    public controllers: any []
    
    public port: number
    
    public ip: any
    
    constructor(controllers: any[], port: any, ip: any)
    
    {
        this.app = express()
        this.port = port
        this.ip = ip
        this.controllers = controllers
        this.initializeMiddlewares()
        this.initializeControllers()
    }

    private initializeMiddlewares()
    {
        this.app.use(cors())
        this.app.use(helmet())
        this.app.use(compression())
        this.app.use(bodyparser.urlencoded({extended: false}))
        this.app.use(bodyparser.json())
        this.app.use( 
            (error: any, request: any, response: any, next: any) => {
                console.error(error.stack)
                response.status(500).send('Something is broken.')
            }
        )    
    }

    private initializeControllers()
    {
        this.controllers.forEach((controller) => {
            this.app.use(
                '/', controller.router
            )
        })
    }

    public listen() 
    {
        this.app.listen(
            this.port,
            this.ip,
            () => {
                console.log(`App is running on: ip:${this.ip} port:${this.port}`)
            }
        )
    }
}

