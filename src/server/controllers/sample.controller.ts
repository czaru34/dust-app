const express = require('express')
import datastore from '../database'
import SampleModel from '../models/sampleModel'
const moment = require('moment')

export class SampleController {
	public router = express.Router()

	constructor() {
		this.initializeRoutes()
	}

	public initializeRoutes() {
		this.router.get('/getServerInfo', this.getInfo)
		this.router.post('/submit', this.submitSample)
		this.router.get('/fetch/all', this.samplesAll)
		this.router.get('/fetch/last', this.fetchLast)
		this.router.get('/fetch/sixtyminutes', this.fetchLastSixtyMinutes)
		this.router.get('/fetch/sixhours', this.fetchLastSixHours)
		this.router.get('/fetch/today', this.fetchToday)
		this.router.get('/fetch/week', this.fetchWeek)
		this.router.get('/fetch/fromto/:from/:to', this.fetchFromTo)
	}

	getInfo = async (req: any, res: any) => {
		res.json({ info: 'hemlo' })
	}

	submitSample = async (req: any, res: any) => {
		try {
			console.log(req.body)
			const data: Array<SampleModel> = req.body
			console.log(data[0])
			const labels = [
				'timestamp',
				'Temperature',
				'Humidity',
				'Pm10',
				'Pm25',
				'Pm100',
				'Particles03',
				'Particles05',
				'Particles10',
				'Particles25',
				'Particles50',
				'Particles100'
			]
			data.forEach((element) => {
				var tempModel: SampleModel = new SampleModel('', '', '', '', '', '', '', '', '', '', '', '')
				var tempElement: SampleModel = new SampleModel(
					element.timestamp,
					element.Temperature,
					element.Humidity,
					element.Pm10,
					element.Pm25,
					element.Pm100,
					element.Particles03,
					element.Particles05,
					element.Particles10,
					element.Particles25,
					element.Particles50,
					element.Particles100
				)
				labels.forEach((label) => {
					if (label === 'timestamp') {
						//console.log(element.timestamp)
						var match = tempElement.timestamp.match(/^(\d{4})\-(\d{2})\-(\d{2}) (\d{2}):(\d{2}):(\d{2})$/)
						if (!(match === null)) {
							var hours = parseInt(match[4], 10)
							var minutes = parseInt(match[5], 10)
							var seconds = parseInt(match[6], 10)
							var year = parseInt(match[1], 10)
							var month = parseInt(match[2], 10) - 1
							var day = parseInt(match[3], 10)

							var dateToCheck = new Date()
							dateToCheck.setFullYear(year, month, day)
							dateToCheck.setHours(hours, minutes, seconds)
							if (
								dateToCheck.getHours() === hours &&
								dateToCheck.getMinutes() === minutes &&
								dateToCheck.getSeconds() === seconds &&
								dateToCheck.getFullYear() === year &&
								dateToCheck.getMonth() === month &&
								dateToCheck.getDate() === day
							)
								tempModel['timestamp'] = tempElement.timestamp
							else {
								console.log('regex fail')
								return false
							}
						} else {
							console.log('match is a null')
							return false
						}
					} else if (isNaN(Number(tempElement.getValue(label)))) {
						console.log('is nan')
						return false
					} else {
						tempModel.setValue(label, tempElement.getValue(label))
					}
				})
				datastore
					.insert({
						timestamp: tempModel.timestamp,
						Temperature: tempModel.Temperature,
						Humidity: tempModel.Humidity,
						Pm10: tempModel.Pm10,
						Pm25: tempModel.Pm25,
						Pm100: tempModel.Pm100,
						Particles03: tempModel.Particles03,
						Particles05: tempModel.Particles05,
						Particles10: tempModel.Particles10,
						Particles25: tempModel.Particles25,
						Particles50: tempModel.Particles50,
						Particles100: tempModel.Particles10
					})
					.then(() => {
						console.log({ status: 'OK' })
					})
					.catch((error: any) => {
						//res.json({ STATUS: 'ERROR', ERROR: JSON.stringify(error) })
						console.log(error)
					})
			})
		} catch (err) {
			console.log(err)
			res.json({ status: `Something went wrong while saving: ${err}.` })
		} finally {
			res.json({ status: 'Everything is okay.' })
		}
	}

	samplesAll = async (req: any, res: any) => {
		datastore
			.find({})
			.sort({ timestamp: -1 })
			.limit(100)
			.then((resp) => {
				res.json(resp)
			})
			.catch((err) => {
				res.json({ error: err })
			})
	}

	fetchLast = async (req: any, res: any) => {
		datastore
			.find({})
			.sort({ timestamp: -1 })
			.limit(1)
			.then((resp) => {
				res.json(resp)
			})
			.catch((err) => {
				res.json({ error: err })
			})
	}

	fetchLastSixtyMinutes = async (req: any, res: any) => {
		datastore
			.find({
				$where: function() {
					const now = moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
					const minusOneHour = moment(new Date()).add(-1, 'h').format('YYYY-MM-DD HH:mm:ss')
					return this.timestamp > minusOneHour && this.timestamp < now
				}
			})
			.sort({ timestamp: -1 })
			.then((resp) => {
				res.json(resp)
			})
			.catch((err) => {
				res.json({ error: err })
			})
	}

	fetchLastSixHours = async (req: any, res: any) => {
		datastore
			.find({
				$where: function() {
					const now = moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
					const minusSixHours = moment(new Date()).add(-6, 'h').format('YYYY-MM-DD HH:mm:ss')
					return this.timestamp > minusSixHours && this.timestamp < now
				}
			})
			.sort({ timestamp: -1 })
			.then((resp) => {
				res.json(resp)
			})
			.catch((err) => console.error(err))
	}

	fetchToday = async (req: any, res: any) => {
		datastore
			.find({
				$where: function() {
					const now = moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
					const minusOneDay = moment(new Date()).add(-1, 'd').format('YYYY-MM-DD HH:mm:ss')
					return this.timestamp > minusOneDay && this.timestamp < now
				}
			})
			.sort({ timestamp: -1 })
			.then((resp) => {
				res.json(resp)
			})
			.catch((err) => console.error(err))
	}

	fetchWeek = async (req: any, res: any) => {
		datastore
			.find({
				$where: function() {
					const now = moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
					const minusSevenDays = moment(new Date()).add(-7, 'd').format('YYYY-MM-DD HH:mm:ss')
					return this.timestamp > minusSevenDays && this.timestamp < now
				}
			})
			.sort({ timestamp: -1 })
			.then((resp) => {
				res.json(resp)
				console.log(resp)
			})
			.catch((err) => console.error(err))
	}

	fetchFromTo = async (req: any, res: any) => {
		console.log(req.params.from + ' - ' + req.params.to)
		if (req.params.from < req.params.to) {
			const from = req.params.from + ' 00:00:00'
			const to = req.params.to + ' 00:00:00'
			datastore
				.find({
					$where: function() {
						return this.timestamp > from && this.timestamp < to
					}
				})
				.sort({ timestamp: 1 })
				.then((resp) => {
					res.json(resp)
					console.log(resp)
				})
				.catch((err) => console.error(err))
		} else {
			console.log('Not this time ;)')
		}
	}
}
