export default class SampleModel {
    timestamp: string
    Temperature: string
    Humidity: string
    Pm10: string
    Pm25: string
    Pm100: string
    Particles03: string
    Particles05: string
    Particles10: string
    Particles25: string
    Particles50: string
    Particles100: string

    cosntructor() {
        this.timestamp = ''
        this.Temperature = ''
        this.Humidity = ''
        this.Pm10 = ''
        this.Pm25 = ''
        this.Pm100 = ''
        this.Particles03 = ''
        this.Particles05 = ''
        this.Particles10 = ''
        this.Particles25 = ''
        this.Particles50 = ''
        this.Particles100 = ''
    }

    constructor(
        timestamp: string,
        Temperature: string,
        Humidity: string,
        Pm10: string,
        Pm25: string,
        Pm100: string,
        Particles03: string,
        Particles05: string,
        Particles10: string,
        Particles25: string,
        Particles50: string,
        Particles100: string
    ) {
        this.timestamp = timestamp
        this.Temperature = Temperature
        this.Humidity = Humidity
        this.Pm10 = Pm10
        this.Pm25 = Pm25
        this.Pm100 = Pm100
        this.Particles03 = Particles03
        this.Particles05 = Particles05
        this.Particles10 = Particles10
        this.Particles25 = Particles25
        this.Particles50 = Particles50
        this.Particles100 = Particles100
    }

    getValue(name: string) {
        switch (name) {
            case 'timestamp': {
                return this.timestamp
                break
            }
            case 'Temperature': {
                return this.Temperature
                break
            }
            case 'Humidity': {
                return this.Humidity
                break
            }
            case 'Pm10': {
                return this.Pm10
                break
            }
            case 'Pm25': {
                return this.Pm25
                break
            }
            case 'Pm100': {
                return this.Pm100
                break
            }
            case 'Particles03': {
                return this.Particles03
                break
            }
            case 'Particles05': {
                return this.Particles05
                break
            }
            case 'Particles10': {
                return this.Particles10
                break
            }
            case 'Particles25': {
                return this.Particles25
                break
            }
            case 'Particles50': {
                return this.Particles50
                break
            }
            case 'Particles100': {
                return this.Particles100
                break
            }
            default: {
                return ''
                break
            }
        }
    }

    setValue(name: string, value: string) {
        switch (name) {
            case 'timestamp': {
                this.timestamp = value
                break
            }
            case 'Temperature': {
                this.Temperature = value
                break
            }
            case 'Humidity': {
                this.Humidity = value
                break
            }
            case 'Pm10': {
                this.Pm10 = value
                break
            }
            case 'Pm25': {
                this.Pm25 = value
                break
            }
            case 'Pm100': {
                this.Pm100 = value
                break
            }
            case 'Particles03': {
                this.Particles03 = value
                break
            }
            case 'Particles05': {
                this.Particles05 = value
                break
            }
            case 'Particles10': {
                this.Particles10 = value
                break
            }
            case 'Particles25': {
                this.Particles25 = value
                break
            }
            case 'Particles50': {
                this.Particles50 = value
                break
            }
            case 'Particles100': {
                this.Particles100 = value
                break
            }
            default: {
                return ''
                break
            }
        }
    }
}