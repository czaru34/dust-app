import * as React from 'react'
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom'
import { Row } from 'reactstrap'
import { Provider } from 'react-redux'
import { applyMiddleware, createStore, compose } from 'redux'
import thunk from 'redux-thunk'
import MyNav from './components/my-nav'
import { Samples } from './components/sample'
import { StatusContainer } from './containers/appstatus'
import { ChartsContainer } from './containers/charts'

import allReducer from './reducers/allReducers'

export type AppState = ReturnType<typeof allReducer>
const store = createStore(allReducer, compose(applyMiddleware(thunk)))

console.log(store.getState())

export const App = (props: any) => {
	return (
		<Provider store={store}>
			<Router>
				<Row className="justify-content-center margin-zero-auto nav-color">
					<MyNav />
				</Row>
				<Switch>
					<Route exact path="/status" component={StatusContainer} />
					<Route exact path="/samples" component={Samples} />
					<Route exact path="/chart" component={ChartsContainer} />
					<Route render={() => <Redirect to="/status" />} />
				</Switch>
			</Router>
		</Provider>
	)
}
