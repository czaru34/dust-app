import statusReducer from './statusReducer'
import chartsReducer from './chartsReducer'
import { combineReducers } from 'redux'

const allReducers = combineReducers({
    status: statusReducer,
    charts: chartsReducer
})

export default allReducers