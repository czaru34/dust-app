import { CSSProperties } from 'react'
import { address } from 'ip'
import { StatusDispatchTypes, STATUS_CONNECT, STATUS_DISCONNECT } from '../actions/StatusActionTypes'

interface IstatusReduxState {
	condition: string
	statusStyle: CSSProperties
	address: string
	ip: string
}

const statusInit: IstatusReduxState = {
	condition: 'Disconnected',
	statusStyle: { color: 'red' },
	address: address(),
	ip: ''
}

const connectReducer = (state: IstatusReduxState = statusInit, action: StatusDispatchTypes): IstatusReduxState => {
	switch (action.type) {
		case STATUS_CONNECT:
			return Object.assign({}, state, {
				condition: 'Connected',
				statusStyle: { color: 'lightgreen' },
				ip: action.ip
			})
		case STATUS_DISCONNECT:
			return Object.assign({}, state, { condition: 'Disconnected', statusStyle: { color: 'red' }, ip: action.ip })
		default:
			return state
	}
}
export default connectReducer
