import * as chartjs from 'chart.js'
import moment from 'moment'
import { GET_LAST_DAY, GET_LAST_WEEK } from '../helpers/fetch'
import {
	ChartsDispatchTypes,
	ADD_TAB,
	REMOVE_TAB,
	TOGGLE_TAB,
	FETCH_DATA_FROM_DB,
	ADD_DROPDOWN,
	REMOVE_DROPDOWN,
	DROPDOWN_SELECT,
	SET_LAST_FETCH_TYPE,
	CHANGE_DATA_COLOR,
	CHANGE_TAB_NAME,
	FROM_SELECT,
	TO_SELECT
} from '../actions/ChartsActionTypes'
import * as rgb from '../helpers/colors'

export const dateFormat = 'YYYY-MM-DD'

export interface ReduxDropdown {
	selected: string
	options: string[]
}

export interface IChartsReduxState {
	id: number
	name: string
	data: chartjs.ChartData
	options: chartjs.ChartOptions
	from: Date
	to: Date
	lastFetchType: string
	selected: string[]
	dropdowns: ReduxDropdown[]
}

interface IchartsInit {
	toggled: string
	chartCount: number
	chartDataColors: rgb.chartDataColor[]
	tabs: IChartsReduxState[]
}

const dropdownOptionsInit: string[] = [
	'Temperature',
	'Humidity',
	'Pm10',
	'Pm25',
	'Pm100',
	'Particles03',
	'Particles05',
	'Particles10',
	'Particles25',
	'Particles50',
	'Particles100'
]

const options: chartjs.ChartOptions = {
	plugins: {
		zoom: {
			pan: {
				enabled: true,
				mode: 'y'
			},
			zoom: {
				enabled: true,
				mode: 'y',
				drag: false,
				sensitivity: 2
			}
		}
	},
	spanGaps: true,
	responsive: true,
	scales: {
		xAxes: [
			{
				type: 'time',
				distribution: 'linear',
				time: {
					parser: 'YYYY-MM-DD HH:mm:SS',
					minUnit: 'minute',
					unit: 'day',
					unitStepSize: 1,
					displayFormats: {
						day: 'YYYY-MM-DD HH:mm:SS',
						minute: 'YYYY-MM-DD HH:mm:SS',
						hour: 'YYYY-MM-DD HH:mm:SS'
					}
				},
				bounds: 'ticks',
				ticks: {
					autoSkip: true,
					display: true,
					fontColor: 'white',
					//stepSize: 3,
					//min: 5,
					maxTicksLimit: 5,
					maxRotation: 0,
					minRotation: 0
				},
				gridLines: {
					zeroLineColor: 'rgba(255, 255, 255, 1)',
					color: 'rgba(255, 255, 255, 0.3)'
				}
			}
		],
		yAxes: [
			{
				type: 'linear',
				ticks: {
					display: true,
					beginAtZero: true,
					fontColor: 'white'
				},
				gridLines: {
					zeroLineColor: 'rgba(255, 255, 255, 1)',
					color: 'rgba(255, 255, 255, 0.3)'
				}
			}
		]
	},
	legend: {
		labels: {
			fontColor: 'white'
		}
	},
	elements: {
		point: {
			radius: 2.5,
			pointStyle: 'rect'
		}
	}
}

const chartsInit: IchartsInit = {
	toggled: '0',
	chartCount: 4,
	chartDataColors: [
		{
			dataType: 'Temperature',
			color: { r: 34, g: 139, b: 34 }
		},
		{
			dataType: 'Humidity',
			color: { r: 10, g: 205, b: 255 }
		},
		{
			dataType: 'Pm10',
			color: { r: 255, g: 0, b: 0 }
		},
		{
			dataType: 'Pm25',
			color: { r: 220, g: 20, b: 60 }
		},
		{
			dataType: 'Pm100',
			color: { r: 240, g: 128, b: 128 }
		},
		{
			dataType: 'Particles03',
			color: { r: 255, g: 204, b: 0 }
		},
		{
			dataType: 'Particles05',
			color: { r: 204, g: 255, b: 51 }
		},
		{
			dataType: 'Particles10',
			color: { r: 255, g: 51, b: 204 }
		},
		{
			dataType: 'Particles25',
			color: { r: 0, g: 255, b: 255 }
		},
		{
			dataType: 'Particles50',
			color: { r: 153, g: 153, b: 102 }
		},
		{
			dataType: 'Particles100',
			color: {
				r: 255,
				g: 255,
				b: 255
			}
		}
	],
	tabs: [
		{
			id: 0,
			name: 'Temperature & humidity',
			data: { labels: [], datasets: [] },
			options,
			lastFetchType: GET_LAST_WEEK,
			from: new Date(),
			to: new Date(),
			selected: [ 'Temperature', 'Humidity' ],
			dropdowns: [
				{
					selected: 'Temperature',
					options: [
						'Pm10',
						'Pm25',
						'Pm100',
						'Particles03',
						'Particles05',
						'Particles10',
						'Particles25',
						'Particles50',
						'Particles100'
					]
				},
				{
					selected: 'Humidity',
					options: [
						'Pm10',
						'Pm25',
						'Pm100',
						'Particles03',
						'Particles05',
						'Particles10',
						'Particles25',
						'Particles50',
						'Particles100'
					]
				}
			]
		},
		{
			id: 1,
			name: "PM's",
			data: { labels: [], datasets: [] },
			options,
			lastFetchType: GET_LAST_WEEK,
			from: new Date(),
			to: new Date(),
			selected: [ 'Pm10', 'Pm25', 'Pm100' ],
			dropdowns: [
				{
					selected: 'Pm10',
					options: [
						'Temperature',
						'Humidity',
						'Particles03',
						'Particles05',
						'Particles10',
						'Particles25',
						'Particles50',
						'Particles100'
					]
				},
				{
					selected: 'Pm25',
					options: [
						'Humidity',
						'Particles03',
						'Particles05',
						'Particles10',
						'Particles25',
						'Particles50',
						'Particles100'
					]
				},
				{
					selected: 'Pm100',
					options: [
						'Humidity',
						'Particles03',
						'Particles05',
						'Particles10',
						'Particles25',
						'Particles50',
						'Particles100'
					]
				}
			]
		},
		{
			id: 2,
			name: 'Particles',
			data: { labels: [], datasets: [] },
			options,
			lastFetchType: GET_LAST_WEEK,
			from: new Date(),
			to: new Date(),
			selected: [ 'Particles03', 'Particles05', 'Particles10', 'Particles25', 'Particles50', 'Particles100' ],
			dropdowns: [
				{
					selected: 'Particles03',
					options: [ 'Temperature', 'Humidity', 'Pm10', 'Pm25', 'Pm100' ]
				},
				{
					selected: 'Particles05',
					options: [ 'Temperature', 'Humidity', 'Pm10', 'Pm25', 'Pm100' ]
				},
				{
					selected: 'Particles10',
					options: [ 'Temperature', 'Humidity', 'Pm10', 'Pm25', 'Pm100' ]
				},
				{
					selected: 'Particles25',
					options: [ 'Temperature', 'Humidity', 'Pm10', 'Pm25', 'Pm100' ]
				},
				{
					selected: 'Particles50',
					options: [ 'Temperature', 'Humidity', 'Pm10', 'Pm25', 'Pm100' ]
				},
				{
					selected: 'Particles100',
					options: [ 'Temperature', 'Humidity', 'Pm10', 'Pm25', 'Pm100' ]
				}
			]
		},
		{
			id: 3,
			name: 'All data',
			data: { labels: [], datasets: [] },
			options,
			lastFetchType: GET_LAST_WEEK,
			from: new Date(),
			to: new Date(),
			selected: [
				'Temperature',
				'Humidity',
				'Pm10',
				'Pm25',
				'Pm100',
				'Particles03',
				'Particles05',
				'Particles10',
				'Particles25',
				'Particles50',
				'Particles100'
			],
			dropdowns: [
				{
					selected: 'Temperature',
					options: []
				},
				{
					selected: 'Humidity',
					options: []
				},
				{
					selected: 'Pm10',
					options: []
				},
				{
					selected: 'Pm25',
					options: []
				},
				{
					selected: 'Pm100',
					options: []
				},
				{
					selected: 'Particles05',
					options: []
				},
				{
					selected: 'Particles10',
					options: []
				},
				{
					selected: 'Particles25',
					options: []
				},
				{
					selected: 'Particles50',
					options: []
				},
				{
					selected: 'Particles100',
					options: []
				}
			]
		}
	]
}

const chartsReducer = (state: IchartsInit, action: ChartsDispatchTypes) => {
	console.log(state)
	switch (action.type) {
		case ADD_TAB: {
			let newChartCount = state.chartCount
			const newTabs = state.tabs
			const count = newChartCount > 0 ? newChartCount + 1 : (newChartCount = 1)
			newTabs.push({
				id: count - 1,
				name: 'Rename me',
				data: { labels: [], datasets: [] },
				options,
				lastFetchType: GET_LAST_DAY,
				from: new Date(),
				to: new Date(),
				selected: new Array<string>(),
				dropdowns: new Array<ReduxDropdown>()
			})
			return {
				...state,
				chartCount: count,
				tabs: newTabs
			}
		}
		case REMOVE_TAB: {
			const newTabs = state.tabs
			const fIndex: number = newTabs
				.map((el) => {
					return el.id
				})
				.indexOf(action.id)
			newTabs.splice(fIndex, 1)
			const remappedTabs = newTabs.filter((val, index) => {
				if (val.id > fIndex) val.id = index

				return val
			})
			const newChartCount = state.chartCount
			if (newChartCount >= 0) newChartCount - 1
			return {
				...state,
				chartCount: newChartCount,
				tabs: remappedTabs
			}
		}
		case TOGGLE_TAB: {
			return {
				...state,
				toggled: action.tab
			}
		}
		case CHANGE_TAB_NAME: {
			const newTabs = state.tabs
			newTabs[action.id].name = action.name
			return {
				...state,
				tabs: newTabs
			}
		}
		case ADD_DROPDOWN: {
			const newTabs = state.tabs
			const selectable: string[] = dropdownOptionsInit.filter(
				(option) => !newTabs[action.id].selected.includes(option)
			)
			if (selectable.length !== 0 && newTabs.length < 11) {
				const tempSelectable: string|undefined = selectable.shift()
				newTabs[action.id].dropdowns.push({
					selected: tempSelectable !== undefined ? tempSelectable : 'Temperature',
					options: selectable
				})
				newTabs[action.id].selected.push(tempSelectable !== undefined ? tempSelectable : 'Temperature')
				newTabs[action.id].dropdowns = newTabs[action.id].dropdowns.filter((item) => {
					item.options = selectable
					return item
				})
			}
			return {
				...state,
				tabs: newTabs
			}
		}
		case REMOVE_DROPDOWN: {
			const newTabs = state.tabs
			newTabs[action.id].dropdowns.pop()
			newTabs[action.id].selected.pop()
			const selectable: string[] = dropdownOptionsInit.filter(
				(option) => !newTabs[action.id].selected.includes(option)
			)
			newTabs[action.id].dropdowns = newTabs[action.id].dropdowns.filter((item) => {
				item.options = selectable
				return item
			})
			return {
				...state,
				tabs: newTabs
			}
		}
		case DROPDOWN_SELECT: {
			const newTabs = state.tabs
			newTabs[action.id].dropdowns[action.index].selected = action.payload
			newTabs[action.id].selected[action.index] = action.payload
			const selectable: string[] = dropdownOptionsInit.filter(
				(option) => !newTabs[action.id].selected.includes(option)
			)
			newTabs[action.id].dropdowns = newTabs[action.id].dropdowns.filter((item) => {
				item.options = selectable
				return item
			})
			return {
				...state,
				tabs: newTabs
			}
		}
		case FROM_SELECT: {
			const newTabs = state.tabs
			newTabs[action.id].from = action.payload
			return {
				...state,
				tabs: newTabs
			}
		}
		case TO_SELECT: {
			const newTabs = state.tabs
			newTabs[action.id].to = action.payload
			return {
				...state,
				tabs: newTabs
			}
		}
		case FETCH_DATA_FROM_DB: {
			const newTabs = state.tabs
			newTabs[action.id].data = action.payload
			const newxAxeOptions = newTabs[action.id].options.scales?.xAxes
			if (newxAxeOptions && newxAxeOptions !== undefined) 
				newxAxeOptions[0].time!.unit = action.unit
			if (newTabs[action.id].options.scales)
				newTabs[action.id].options.scales!.xAxes = newxAxeOptions
			return {
				...state,
				tabs: newTabs
			}
		}
		case SET_LAST_FETCH_TYPE: {
			console.log(`Last fetch type: ${action.payload}`)
			const newTabs = state.tabs
			newTabs[action.id].lastFetchType = action.payload
			return {
				...state,
				tabs: newTabs
			}
		}
		case CHANGE_DATA_COLOR: {
			const newColors = state.chartDataColors
			newColors.forEach((color) => {
				if (color.dataType === action.payload.dataType) {
					color.color = action.payload.color
					return color
				}
				return null
			})
			return {
				...state,
				chartDataColors: newColors
			}
		}
		default:
			return chartsInit
	}
}

export default chartsReducer
