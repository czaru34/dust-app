import { Dispatch } from 'redux'
import { address } from 'ip'
import moment from 'moment'
import axios, { AxiosError, AxiosResponse } from 'axios'
import * as chartjs from 'chart.js'
import {
	ADD_TAB,
	REMOVE_TAB,
	ChartsDispatchTypes,
	TOGGLE_TAB,
	FETCH_DATA_FROM_DB,
	ADD_DROPDOWN,
	REMOVE_DROPDOWN,
	DROPDOWN_SELECT,
	FROM_SELECT,
	TO_SELECT,
	SET_LAST_FETCH_TYPE,
	CHANGE_DATA_COLOR,
	CHANGE_TAB_NAME
} from './ChartsActionTypes'
import { GET_LAST_60, GET_LAST_6, GET_LAST_DAY, GET_LAST_WEEK, GET_FROM_TO } from '../helpers/fetch'

import Sample from '../components/sampleDefinition'

import * as rgb from '../helpers/colors'

export const AddTab = (id: number) => {
	return (dispatch: Dispatch<ChartsDispatchTypes>) => {
		dispatch({ type: ADD_TAB })
	}
}

export const RemoveTab = (id: number) => {
	return (dispatch: Dispatch<ChartsDispatchTypes>) => {
		dispatch({ type: REMOVE_TAB, id })
	}
}

export const ChangeTabName = (id: number, name: string) => {
	return (dispatch: Dispatch<ChartsDispatchTypes>) => {
		dispatch({ type: CHANGE_TAB_NAME, id, name })
	}
}

export const Toggle = (tab: string) => {
	return (dispatch: Dispatch<ChartsDispatchTypes>) => {
		dispatch({ type: TOGGLE_TAB, tab })
	}
}

export const AddDropdown = (id: number) => {
	return (dispatch: Dispatch<ChartsDispatchTypes>) => {
		dispatch({ type: ADD_DROPDOWN, id })
	}
}

export const RemoveDropdown = (id: number) => {
	return (dispatch: Dispatch<ChartsDispatchTypes>) => {
		dispatch({ type: REMOVE_DROPDOWN, id })
	}
}

export const DropdownSelect = (id: number, index: number, payload: string) => {
	return (dispatch: Dispatch<ChartsDispatchTypes>) => {
		dispatch({ type: DROPDOWN_SELECT, id, index, payload })
	}
}

export const FromSelect = (id: number, payload: Date) => {
	return (dispatch: Dispatch<ChartsDispatchTypes>) => {
		dispatch({ type: FROM_SELECT, id, payload })
	}
}

export const ToSelect = (id: number, payload: Date) => {
	return (dispatch: Dispatch<ChartsDispatchTypes>) => {
		dispatch({ type: TO_SELECT, id, payload })
	}
}

export const Fetch = (
	id: number,
	selected: string[],
	typeOfFetch: string,
	chartDataColors: rgb.chartDataColor[],
	from?: string,
	to?: string
) => {
	let unit = ''
	let url = ''
	switch (typeOfFetch) {
		case GET_LAST_60:
			{
				url = 'http://' + address() + ':80/fetch/sixtyminutes'
				unit = 'minute'
			}
			break
		case GET_LAST_6:
			{
				url = 'http://' + address() + ':80/fetch/sixhours'
				unit = 'hour'
			}
			break
		case GET_LAST_DAY:
			{
				url = 'http://' + address() + ':80/fetch/today'
				unit = 'hour'
			}
			break
		case GET_LAST_WEEK:
			{
				url = 'http://' + address() + ':80/fetch/week'
				unit = 'day'
			}
			break
		case GET_FROM_TO:
			{
				url = 'http://' + address() + `:80/fetch/fromto/${from}/${to}`
				const to_ms = moment(to).valueOf()
				const from_ms = moment(from).valueOf()
				const diff_min = Math.ceil(to_ms - from_ms) / 60000
				if (diff_min > 1440 && diff_min <= 10080) unit = 'day'
				else if (diff_min > 10080 && diff_min <= 43829) unit = 'month'
				else if (diff_min > 43829 && diff_min <= 525948) unit = 'year'
				else unit = 'hour'
			}
			break
		default:
			{
				url = 'http://' + address() + ':80/fetch/today'
				unit = 'hour'
			}
			break
	}

	return (dispatch: Dispatch<ChartsDispatchTypes>): void => {
		const promise: Promise<void> = axios
			.get(url)
			.then((response: AxiosResponse) => {
				console.log('Unit:' + unit)
				const rawData: any = response.data
				rawData.forEach((val: any) => delete val._id)
				const newSamples: Sample[] = rawData
				const newLabels: string[] = []
				let newData: Array<any> = []
				const newDatasets: Array<any> = []
				let i = 0

				selected.forEach((dataType: string) => {
					newSamples.forEach((sample: Sample, index1: number) => {
						const newSamplePart = sample[dataType]
						newData[index1] = {
							t: moment(new Date(sample['timestamp'])).format('YYYY-MM-DD HH:mm:ss'),
							y: parseInt(newSamplePart, 10)
						}
					})
					const color = chartDataColors.filter((element) => {
						if (element.dataType === dataType) return element
					})
					newDatasets.push({
						label: dataType.valueOf(),
						data: newData,
						borderColor: rgb.getRgbaString(color[0].color),
						lineTension: 0,
						fill: false
					})
					newData = []
					i = 0
				})

				i = 0
				newSamples.forEach((sample: Sample) => {
					newLabels[i] = sample['timestamp']
					i += 1
				})
				const data: chartjs.ChartData = { labels: newLabels, datasets: newDatasets }
				dispatch({ type: FETCH_DATA_FROM_DB, id, payload: data, unit })
				return null
			})
			.catch((error) => {
				console.error(`There was an error while ${typeOfFetch}: ${error}`)
			})
	}
}

export const SetLastFetchType = (id: number, typeOfFetch: string) => {
	return (dispatch: Dispatch<ChartsDispatchTypes>) => {
		dispatch({ type: SET_LAST_FETCH_TYPE, id, payload: typeOfFetch })
	}
}

export const ChangeDataColor = (color: rgb.chartDataColor) => {
	return (dispatch: Dispatch<ChartsDispatchTypes>) => {
		dispatch({ type: CHANGE_DATA_COLOR, payload: color })
	}
}
