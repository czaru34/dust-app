import * as chartjs from 'chart.js'
import * as rgb from '../helpers/colors'

export const ADD_TAB = 'ADD_TAB'
export const REMOVE_TAB = 'REMOVE_TAB'
export const TOGGLE_TAB = 'TOGGLE_TAB'
export const CHANGE_TAB_NAME = 'CHANGE_TAB_NAME'
export const ADD_DROPDOWN = 'ADD_DROPDOWN'
export const REMOVE_DROPDOWN = 'REMOVE_DROPDOWN'
export const DROPDOWN_SELECT = 'DROPDOWN_SELECT'
export const FROM_SELECT = 'FROM_SELECT'
export const TO_SELECT = 'TO_SELECT'
export const FETCH_DATA_FROM_DB = 'FETCH_DATA_FROM_DB'
export const SET_LAST_FETCH_TYPE = 'SET_LAST_FETCH_TYPE'
export const CHANGE_DATA_COLOR = 'CHANGE_DATA_COLOR'

export interface AddTab {
	type: typeof ADD_TAB
}
export interface RemoveTab {
	type: typeof REMOVE_TAB
	id: number
}

export interface ToggleTab {
	type: typeof TOGGLE_TAB
	tab: string
}

export interface ChangeTabName {
	type: typeof CHANGE_TAB_NAME
	id: number
	name: string
}

export interface AddDropdown {
	type: typeof ADD_DROPDOWN
	id: number
}

export interface RemoveDropdown {
	type: typeof REMOVE_DROPDOWN
	id: number
}

export interface DropdownSelect {
	type: typeof DROPDOWN_SELECT
	id: number
	index: number
	payload: string
}

export interface FromSelect {
	type: typeof FROM_SELECT
	id: number
	payload: Date
}

export interface ToSelect {
	type: typeof TO_SELECT
	id: number
	payload: Date
}

export interface FetchDataFromDb {
	type: typeof FETCH_DATA_FROM_DB
	id: number
	payload: chartjs.ChartData
	unit: string
}

export interface SetLastFetchType {
	type: typeof SET_LAST_FETCH_TYPE
	id: number
	payload: string
}

export interface ChangeDataColor {
	type: typeof CHANGE_DATA_COLOR
	payload: rgb.chartDataColor
}

export type ChartsDispatchTypes =
	| AddTab
	| RemoveTab
	| ToggleTab
	| ChangeTabName
	| AddDropdown
	| RemoveDropdown
	| DropdownSelect
	| FromSelect
	| ToSelect
	| FetchDataFromDb
	| SetLastFetchType
	| ChangeDataColor
