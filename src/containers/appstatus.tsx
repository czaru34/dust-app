import * as React from 'react'
import { Component } from 'react'
import { StatusComponent } from '../components/status'
import { connect } from 'react-redux'
import { AppState } from '../App'
import { StatusDispatchTypes } from '../actions/StatusActionTypes'
import { ThunkDispatch } from 'redux-thunk'
import { Connect } from '../actions/StatusActions'

class AppStatus extends Component<Props, StatusState> {
	constructor(props: any) {
		super(props)
		this.state = {
			status: null
		}
	}

	componentDidMount() {
		console.log(this.props.status)
		this.setState({
			status: this.props.status
		})
		if (this.props.status.ip !== '') this.props.handleConnectClick(this.props.status.ip)
	}

	handleIpInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
		this.setState({
			...this.state,
			status: {
				...this.state.status,
				ip: event.target.value
			}
		})
	}

	handleSubmit = (event: React.FormEvent) => {
		event.preventDefault()
	}

	handleConnect = () => {
		this.props.handleConnectClick(this.state.status.ip!)
		console.log(this.props)
	}

	render() {
		return (
			<StatusComponent
				ip={this.props.status.ip}
				address={this.props.status.address}
				status={this.props.status.condition}
				statusStyle={this.props.status.statusStyle}
				onChange={this.handleIpInputChange}
				onClick={this.handleConnect}
				onSubmit={this.handleSubmit}
			/>
		)
	}
}

// ! Props
type Props = StateProps & DispatchProps & StatusProps

// * The type for the props provided by the parent component

export interface StatusProps {
	status: {
		condition: string
		statusStyle: React.CSSProperties
		address: string
		ip: string
	}
}

export interface StatusState {
	status: {
		condition: string
		statusStyle: React.CSSProperties
		address: string
		ip: string
	}
}

// * typeof props from redux store (The type for the props provided by mapStateToProps())
interface StateProps {
	status: {
		condition: string
		statusStyle: React.CSSProperties
		address: string
		ip: string
	}
}

// * typeof methods from redux store (The type for the props provided by mapDispatchToProps())

interface DispatchProps {
	handleConnectClick: (ip: string) => void
}

function mapStateToProps(state: AppState): StateProps {
	return {
		status: state.status
	}
}

function mapDispatchToProps(dispatch: ThunkDispatch<AppState, null, StatusDispatchTypes>): DispatchProps {
	return {
		handleConnectClick: (ip: string) => dispatch(Connect(ip))
	}
}

export const StatusContainer = connect<StateProps, DispatchProps>(mapStateToProps, mapDispatchToProps)(AppStatus)
