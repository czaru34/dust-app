import * as React from 'react'
import { connect } from 'react-redux'
import { ThunkDispatch } from 'redux-thunk'
import { AppState } from '../App'
import { ChartsDispatchTypes } from '../actions/ChartsActionTypes'
import {
	Toggle,
	AddTab,
	RemoveTab,
	AddDropdown,
	RemoveDropdown,
	Fetch,
	DropdownSelect,
	SetLastFetchType,
	ChangeDataColor,
	ChangeTabName,
	FromSelect,
	ToSelect
} from '../actions/ChartsActions'
import { CharTab } from '../components/chartab'
import { IChartsReduxState } from '../reducers/chartsReducer'
import * as rgb from '../helpers/colors'

class Charts extends React.Component<Props, ChartsState> {
	constructor(props: any) {
		super(props)
	}

	componentDidMount() {
		this.setState({
			charts: this.props.charts
		})
	}

	toggle = (event: React.MouseEvent<any, MouseEvent>) => {
		this.props.handleToggle(event.currentTarget.id)
	}

	incrementTabs = () => {
		this.props.addTab(0)
	}

	removeTab = (id: number) => {
		this.props.removeTab(id)
	}

	render() {
		return (
			<CharTab
				chartDataColors={this.props.charts.chartDataColors}
				tabs={this.props.charts.tabs}
				toggled={this.props.charts.toggled}
				tuggle={this.toggle}
				addTab={this.incrementTabs}
				removeTab={this.removeTab}
				changeTabName={this.props.changeTabName}
				addDropdown={this.props.addDropdown}
				removeDropdown={this.props.removeDropdown}
				dropdownSelect={this.props.dropdownSelect}
				fromSelect={this.props.fromSelect}
				toSelect={this.props.toSelect}
				fetch={this.props.fetch}
				setLastFetchType={this.props.setLastFetchType}
				changeDataColor={this.props.changeDataColor}
			/>
		)
	}
}

// ! Props

type Props = ChartsProps & StateProps & DispatchProps

// *
export interface ChartsProps {
	charts: {
		toggled: string
		chartDataColors: rgb.chartDataColor[]
		tabs: IChartsReduxState[]
	}
}

export interface ChartsState {
	charts: {
		toggled: string
		chartDataColors: rgb.chartDataColor[]
		tabs: IChartsReduxState[]
	}
}

interface StateProps {
	charts: {
		toggled: string
		chartDataColors: rgb.chartDataColor[]
		tabs: IChartsReduxState[]
	}
}

interface DispatchProps {
	handleToggle: (tab: string) => void
	addTab: (id: number) => void
	removeTab: (id: number) => void
	changeTabName: (id: number, name: string) => void
	addDropdown: (id: number) => void
	removeDropdown: (id: number) => void
	dropdownSelect: (id: number, index: number, payload: string) => void
	fromSelect: (id: number, payload: Date) => void
	toSelect: (id: number, payload: Date) => void
	fetch: (
		id: number,
		selected: string[],
		typeOfFetch: string,
		chartDataColors: rgb.chartDataColor[],
		from?: string,
		to?: string
	) => void
	setLastFetchType: (id: number, typeOfFetch: string) => void
	changeDataColor: (color: rgb.chartDataColor) => void
}

function mapStateToProps(state: AppState): StateProps {
	return {
		charts: state.charts
	}
}

function mapDispatchToProps(dispatch: ThunkDispatch<AppState, {}, ChartsDispatchTypes>): DispatchProps {
	return {
		handleToggle: (tab: string) => dispatch(Toggle(tab)),
		addTab: (id: number) => dispatch(AddTab(id)),
		removeTab: (id: number) => dispatch(RemoveTab(id)),
		changeTabName: (id: number, name: string) => dispatch(ChangeTabName(id, name)),
		addDropdown: (id: number) => dispatch(AddDropdown(id)),
		removeDropdown: (id: number) => dispatch(RemoveDropdown(id)),
		dropdownSelect: (id: number, index: number, payload: string) => dispatch(DropdownSelect(id, index, payload)),
		fromSelect: (id: number, payload: Date) => dispatch(FromSelect(id, payload)),
		toSelect: (id: number, payload: Date) => dispatch(ToSelect(id, payload)),
		fetch: (
			id: number,
			selected: string[],
			typeOfFetch: string,
			chartDataColors: rgb.chartDataColor[],
			from?: string,
			to?: string
		) => dispatch(Fetch(id, selected, typeOfFetch, chartDataColors, from, to)),
		setLastFetchType: (id: number, typeOfFetch: string) => dispatch(SetLastFetchType(id, typeOfFetch)),
		changeDataColor: (color: rgb.chartDataColor) => dispatch(ChangeDataColor(color))
	}
}

export const ChartsContainer = connect<StateProps, DispatchProps>(mapStateToProps, mapDispatchToProps)(Charts)
