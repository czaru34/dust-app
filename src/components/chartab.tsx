import * as React from 'react'
import { Row, Col, NavItem, Container, TabPane, TabContent, Nav, NavLink } from 'reactstrap'
import classnames from 'classnames'
import { BsFillPlusSquareFill } from 'react-icons/bs'
import moment from 'moment'
import { IChartsReduxState, dateFormat } from '../reducers/chartsReducer'

import { ChartPanel } from './chartpanel'
import TabName from './tabname'
import * as rgb from '../helpers/colors'

interface CharTabProps {
	toggled: string
	tabs: IChartsReduxState[]
	chartDataColors: rgb.chartDataColor[]
	tuggle: (event: React.MouseEvent<any, MouseEvent>) => void
	addTab: () => void
	removeTab: (id: number) => void
	addDropdown: (id: number) => void
	changeTabName: (id: number, name: string) => void
	removeDropdown: (id: number) => void
	dropdownSelect: (id: number, index: number, payload: string) => void
	fromSelect: (id: number, payload: Date) => void
	toSelect: (id: number, payload: Date) => void
	fetch: (
		id: number,
		selected: string[],
		typeOfFetch: string,
		chartDataColors: rgb.chartDataColor[],
		from?: string,
		to?: string
	) => void
	setLastFetchType: (id: number, typeOfFetch: string) => void
	changeDataColor: (color: rgb.chartDataColor) => void
}

export const CharTab = (props: CharTabProps) => {
	return (
		<Row className="margin-zero-auto">
			<Container fluid className="margin-top-40">
				<Row className="justify-content-center">
					<Col>
						<Nav tabs>
							{props.tabs.map((element) => {
								return (
									<NavItem key={element.id.toString()}>
										<NavLink
											id={element.id.toString()}
											className={classnames(
												{
													active: props.toggled === element.id.toString()
												},
												{
													'justify-content-center': true
												},
												{
													'align-items-center': true
												},
												{
													'd-flex': true
												}
											)}
											onClick={(event) => {
												props.tuggle(event)
												props.fetch(
													element.id,
													element.selected,
													element.lastFetchType,
													props.chartDataColors,
													moment(element.from).format(dateFormat),
													moment(element.to).format(dateFormat)
												)
											}}
										>
											<TabName
												id={element.id}
												name={element.name}
												removeTab={props.removeTab}
												changeTabName={props.changeTabName}
											/>
										</NavLink>
									</NavItem>
								)
							})}
							<NavItem key="addChartPanel">
								<NavLink
									className={classnames(
										{
											'justify-content-center': true
										},
										{
											'align-items-center': true
										},
										{
											'd-flex': true
										}
									)}
								>
									<span role="button" className="tab-add" onClick={props.addTab}>
										<BsFillPlusSquareFill />
									</span>
								</NavLink>
							</NavItem>
						</Nav>
					</Col>
				</Row>
				<Row className="justify-content-center">
					<Col>
						<TabContent activeTab={props.toggled} className="justify-content-center">
							{props.tabs.map((element) => {
								return (
									<TabPane key={element.id.toString()} tabId={element.id.toString()}>
										<ChartPanel
											chartDataColors={props.chartDataColors}
											dropdowns={element.dropdowns}
											id={element.id}
											data={element.data}
											options={element.options}
											lastFetchType={element.lastFetchType}
											selected={element.selected}
											from={element.from}
											to={element.to}
											addDropdown={props.addDropdown}
											removeDropdown={props.removeDropdown}
											dropdownSelect={props.dropdownSelect}
											fromSelect={props.fromSelect}
											toSelect={props.toSelect}
											fetch={props.fetch}
											setLastFetchType={props.setLastFetchType}
											changeDataColor={props.changeDataColor}
										/>
									</TabPane>
								)
							})}
						</TabContent>
					</Col>
				</Row>
			</Container>
		</Row>
	)
}

CharTab.displayName = 'CharTab'
