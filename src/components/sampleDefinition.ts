export default class Sample {
	timestamp: string

	Temperature: string

	Humidity: string

	Pm10: string

	Pm25: string

	Pm100: string

	Particles03: string

	Particles05: string

	Particles10: string

	Particles25: string

	Particles50: string

	Particles100: string

	constructor(json: any) {
		this.timestamp = json.timestamp
		this.Temperature = json.temperature
		this.Humidity = json.humidity
		this.Pm10 = json.pm10standard
		this.Pm25 = json.pm25standard
		this.Pm100 = json.pm100standard
		this.Particles03 = json.particles03
		this.Particles05 = json.particles05
		this.Particles10 = json.particles10
		this.Particles25 = json.particles25
		this.Particles50 = json.particles50
		this.Particles100 = json.particles100
	}

	getElement(name: string): string {
		switch (name) {
			case 'timestamp': {
				return this.timestamp
				break
			}
			case 'Temperature': {
				return this.Temperature
				break
			}
			case 'Humidity': {
				return this.Humidity
				break
			}
			case 'Pm10': {
				return this.Pm10
				break
			}
			case 'Pm25': {
				return this.Pm25
				break
			}
			case 'Pm100': {
				return this.Pm100
				break
			}
			case 'Particles03': {
				return this.Particles03
				break
			}
			case 'Particles05': {
				return this.Particles05
				break
			}
			case 'Particles10': {
				return this.Particles10
				break
			}
			case 'Particles25': {
				return this.Particles25
				break
			}
			case 'Particles50': {
				return this.Particles50
				break
			}
			case 'Particles100': {
				return this.Particles100
				break
			}
			default: {
				return ''
				break
			}
		}
	}
}
