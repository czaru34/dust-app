import * as React from 'react'
import axios from 'axios'

import { SampleList } from './sample-list'
import { address } from 'ip'

export const Samples = () => {
	const [ samples, setSamples ] = React.useState([])
	const [ loading, setLoading ] = React.useState(true)

	React.useEffect(() => {
		fetchSamples()
	}, [])

	const fetchSamples = async () => {
		axios
			.get('http://' + address() + ':80/fetch/all')
			.then((response) => {
				console.log(response.data)
				setSamples(response.data)
				setLoading(false)
			})
			.catch((error) => console.error(`There was an error retrieving the sample list: ${error}`))
	}

	return <SampleList samples={samples} loading={loading} />
}
