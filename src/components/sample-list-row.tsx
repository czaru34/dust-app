import * as React from 'react'

interface SampleListRowUI {
  position: number
  sample: {
    _id: string
    timestamp: string
    Temperature: number
    Humidity: number
    Pm10: number
    Pm25: number
    Pm100: number
    Particles03: number
    Particles05: number
    Particles10: number
    Particles25: number
    Particles50: number
    Particles100: number
  }
}

export const SampleListRow = (props: SampleListRowUI) => (
  <tr >
    <td >
      {props.sample.timestamp}
    </td>
    <td className='bigger-text'>
      {props.sample.Temperature}
    </td>
    <td className='bigger-text'>
      {props.sample.Humidity}
    </td>
    <td className='bigger-text'>
      {props.sample.Pm10}
    </td>
    <td className='bigger-text'>
      {props.sample.Pm25}
    </td>
    <td className='bigger-text'>
      {props.sample.Pm100}
    </td>
    <td className='bigger-text'>
      {props.sample.Particles03}
    </td>
    <td className='bigger-text'>
      {props.sample.Particles05}
    </td>
    <td className='bigger-text'>
      {props.sample.Particles10}
    </td>
    <td className='bigger-text'>
      {props.sample.Particles25}
    </td>
    <td className='bigger-text'>
      {props.sample.Particles50}
    </td>
    <td className='bigger-text'>
      {props.sample.Particles100}
    </td>
  </tr>
)