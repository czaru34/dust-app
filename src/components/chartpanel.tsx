import * as React from 'react'
import { Container, Row, Col, Button, ButtonGroup } from 'reactstrap'
import DatePicker from 'react-datepicker'
import 'hammerjs'
import 'chartjs-plugin-zoom'
import * as chartjs from 'chart.js'
import { Line } from 'react-chartjs-2'
import moment from 'moment'
import { GET_LAST_60, GET_LAST_6, GET_LAST_DAY, GET_LAST_WEEK, GET_FROM_TO } from '../helpers/fetch'
import DataDropdown from './datadropdown'
import { ReduxDropdown } from '../reducers/chartsReducer'
import * as rgb from '../helpers/colors'
import ColorSettings from './colorsettings'

interface IChartPanel {
	chartDataColors: rgb.chartDataColor[]
	id: number
	data: chartjs.ChartData
	options: chartjs.ChartOptions
	lastFetchType: string
	selected: string[]
	from: Date
	to: Date
	dropdowns: Array<ReduxDropdown>
	addDropdown: (id: number) => void
	removeDropdown: (id: number) => void
	dropdownSelect: (id: number, index: number, payload: string) => void
	fromSelect: (id: number, payload: Date) => void
	toSelect: (id: number, payload: Date) => void
	fetch: (
		id: number,
		selected: string[],
		typeOfFetch: string,
		chartDataColors: rgb.chartDataColor[],
		from?: string,
		to?: string
	) => void
	setLastFetchType: (id: number, typeOfFetch: string) => void
	changeDataColor: (color: rgb.chartDataColor) => void
}

export const ChartPanel = (props: IChartPanel) => {
	const chartRef: React.RefObject<Line> = React.createRef<Line>()

	const refs: React.RefObject<DataDropdown>[] = []
	const dateFormat = 'yyyy-MM-D'

	const dropdowns = props.dropdowns.map((element, index) => {
		const ref = React.createRef<DataDropdown>()
		refs.push(ref)
		return (
			<div key={element.selected} className="d-flex justify-content-center">
				<DataDropdown
					chartId={props.id}
					ref={ref}
					index={index}
					chartDataColors={props.chartDataColors}
					options={element.options}
					allDropdownsSelected={props.selected}
					title={element.selected}
					lastFetchType={props.lastFetchType}
					fromSelected={moment(props.from).format(dateFormat)}
					toSelected={moment(props.to).format(dateFormat)}
					selected={element.selected}
					dropdownSelect={props.dropdownSelect}
					fetch={props.fetch}
				/>
			</div>
		)
	})

	return (
		<Container fluid className="margin-top-20">
			<Row className="d-flex justify-content-center">
				<Col>
					<Button
						color="warning"
						block
						onClick={() => {
							const currentChart = chartRef.current?.chartInstance!
							//@ts-ignore
							currentChart.resetZoom()
						}}
					>
						RESET ZOOM
					</Button>
				</Col>
				<Col>
					<ColorSettings
						chartDataColors={props.chartDataColors}
						chartId={props.id}
						allDropdownsSelected={props.selected}
						lastFetchType={props.lastFetchType}
						fromSelected={moment(props.from).format(dateFormat)}
						toSelected={moment(props.to).format(dateFormat)}
						colors={props.chartDataColors}
						changeDataColor={props.changeDataColor}
						fetch={props.fetch}
					/>
				</Col>
			</Row>
			<Row>
				<Line ref={chartRef} data={props.data} options={props.options} />
			</Row>
			<Row className="d-flex justify-content-center">
				<ButtonGroup className="btngroup-minimized">
					<Button
						color="danger"
						onClick={() => {
							props.removeDropdown(props.id)
							props.fetch(
								props.id,
								props.selected,
								props.lastFetchType,
								props.chartDataColors,
								moment(props.from).format(dateFormat),
								moment(props.to).format(dateFormat)
							)
						}}
					>
						-
					</Button>
					{dropdowns}
					<Button
						color="success"
						onClick={() => {
							props.addDropdown(props.id)
							props.fetch(
								props.id,
								props.selected,
								props.lastFetchType,
								props.chartDataColors,
								moment(props.from).format(dateFormat),
								moment(props.to).format(dateFormat)
							)
						}}
					>
						+
					</Button>
				</ButtonGroup>
			</Row>
			<Row className="mt-3">
				<Col className="col-md-3 col-sm-4 col-lg" style={{ textAlign: 'center', margin: 'auto' }}>
					<DatePicker
						selected={props.from}
						dateFormat="yyyy-MM-dd"
						popperPlacement="top"
						onChange={(date: any) => {
							props.fromSelect(props.id, date)
							props.setLastFetchType(props.id, GET_FROM_TO)
							props.fetch(
								props.id,
								props.selected,
								props.lastFetchType,
								props.chartDataColors,
								moment(props.from).format(dateFormat),
								moment(props.to).format(dateFormat)
							)
						}}
					/>
				</Col>
				<Col className="col-md-3 col-sm-4 col-lg">
					<Button
						color="warning"
						block
						onClick={() => {
							props.setLastFetchType(props.id, GET_FROM_TO)
							props.fetch(
								props.id,
								props.dropdowns.map((dropdown) => {
									return dropdown.selected
								}),
								'GET_FROM_TO',
								props.chartDataColors,
								moment(props.from).format(dateFormat),
								moment(props.to).format(dateFormat)
							)
						}}
					>
						SHOW
					</Button>
				</Col>
				<Col className="col-md-3 col-sm-4 col-lg" style={{ textAlign: 'center', margin: 'auto' }}>
					<DatePicker
						selected={props.to}
						dateFormat="yyyy-MM-dd"
						popperPlacement="top"
						onChange={(date: any) => {
							props.toSelect(props.id, date)
							props.setLastFetchType(props.id, GET_FROM_TO)
							props.fetch(
								props.id,
								props.selected,
								props.lastFetchType,
								props.chartDataColors,
								moment(props.from).format(dateFormat),
								moment(props.to).format(dateFormat)
							)
						}}
					/>
				</Col>
			</Row>
			<Row className="mt-3">
				<Col md>
					<Button
						color="primary"
						onClick={() => {
							props.setLastFetchType(props.id, GET_LAST_60)
							props.fetch(
								props.id,
								props.dropdowns.map((dropdown) => dropdown.selected),
								'GET_LAST_60',
								props.chartDataColors
							)
						}}
						block
					>
						LAST 60 MINUTES
					</Button>
				</Col>
				<Col md>
					<Button
						color="primary"
						onClick={() => {
							props.setLastFetchType(props.id, GET_LAST_6)
							props.fetch(
								props.id,
								props.dropdowns.map((dropdown) => dropdown.selected),
								'GET_LAST_6',
								props.chartDataColors
							)
						}}
						block
					>
						LAST 6 HOURS
					</Button>
				</Col>
				<Col md>
					<Button
						color="primary"
						onClick={() => {
							props.setLastFetchType(props.id, GET_LAST_DAY)
							props.fetch(
								props.id,
								props.dropdowns.map((dropdown) => dropdown.selected),
								'GET_LAST_DAY',
								props.chartDataColors
							)
						}}
						block
					>
						LAST 24 HOURS
					</Button>
				</Col>
				<Col md>
					<Button
						color="primary"
						onClick={() => {
							props.setLastFetchType(props.id, GET_LAST_WEEK)
							props.fetch(
								props.id,
								props.dropdowns.map((dropdown) => dropdown.selected),
								'GET_LAST_WEEK',
								props.chartDataColors
							)
						}}
						block
					>
						LAST WEEK
					</Button>
				</Col>
			</Row>
		</Container>
	)
}
