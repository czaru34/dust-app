// src/electron.js
import { app, BrowserWindow } from 'electron'
import App from './server/app'
import { SampleController } from './server/controllers/sample.controller'
const sourceMapSupport = require('source-map-support')
require('babel-polyfill')

const ip = require('ip')

if (process.platform === 'linux') app.commandLine.appendSwitch('--no-sandbox')

const application: App = new App([ new SampleController() ], 80, ip.address())

application.listen()

if (process.env.NODE_ENV === 'production') {
	sourceMapSupport.install()
}

app.on('ready', async () => {
	const win = new BrowserWindow({
		width: 1600,
		height: 900,
		title: 'DustApp',
		frame: true,
		webPreferences: {
			worldSafeExecuteJavaScript: true,
			nodeIntegration: true
		}
	})
	await win.loadURL(`file://${__dirname}/index.html`)
	win.webContents.on('did-finish-load', () => {
		if (!win) {
			throw new Error('"mainWindow" is not defined')
		}
		if (process.env.START_MINIMIZED) {
			win.minimize()
		} else {
			win.show()
			win.focus()
		}
	})
})
