module.exports = {
	extends: 'erb',
	rules: {
		// A temporary hack related to IDE not resolving correct package.json
		'import/no-extraneous-dependencies': 'off',
		'import/prefer-default-export': 0,
		'prettier/prettier': 0,
		'react/destructuring-assignment': [ 0 ],
		semi: 0,
		'@typescript-eslint/semi': 'off',
		avoidEscape: true,
		allowTemplateLiterals: true
	},
	parserOptions: {
		ecmaVersion: 2020,
		sourceType: 'module',
		project: './tsconfig.json',
		tsconfigRootDir: __dirname,
		createDefaultProgram: true
	},
	settings: {
		'import/resolver': {
			// See https://github.com/benmosher/eslint-plugin-import/issues/1396#issuecomment-575727774 for line below
			node: {},
			webpack: {
				config: require.resolve('./.erb/configs/webpack.config.eslint.js')
			}
		},
		'import/parsers': {
			'@typescript-eslint/parser': [ '.ts', '.tsx' ]
		}
	}
}
